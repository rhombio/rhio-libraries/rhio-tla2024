#include <Arduino.h>
#include <Wire.h>

#include "rhio-tla2024.h"

TLA2024 tla = TLA2024();
void setup() {
  SerialUSB.begin(9800);
  while (!SerialUSB) {
  }
  delay(5000);
  SerialUSB.println(F("START"));
  while (!tla.begin(ADDR_1)) {
  }
}

void loop() {
  SerialUSB.print(F("VBAT="));
  SerialUSB.println((tla.getVbat()));
  delay(1000);

  SerialUSB.print(F("VIN="));
  SerialUSB.println(tla.getVin());
  delay(1000);

  SerialUSB.print(F("VSYS="));
  SerialUSB.println(tla.getVsys());
  delay(1000);

  SerialUSB.print(F("\n"));
  delay(1000);
}
