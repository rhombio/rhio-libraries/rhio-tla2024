# rhio-tla2024
 
This is a portable library based on Arduino to work with the TLA2024. It was developed by Rhomb.io and it is open source. The TLA2024 is a 12-Bit Delta-Sigma ADC made by Texas Instruments. This ADC is the one mounted in the Halley Box carrier board from Rhomb.io. This is why this library has some specific functions to make the most for this board.
 
## Features
 
* I2C Interface
* 4 Single-Ended or 2 Differential Inputs
* Programmable Gain Amplifier
 
## Main functions
 
* begin() : it initialites the default configuration of the TLA2024 and makes sure it is well connected.
* getAnalog(muxOption) : as its name shows, it allows the user to get an analog measure of the selected muxOption. Depending on this parameter, this analog measure will be differential (AIN0AIN1, AIN0AIN3,  AIN1AIN3 or AIN2AIN3) or single-ended  (AIN0,  AIN1,  AIN2 or  AIN3). All this options are defined on rhio-tla2024.h and are referenced to the TLA2024's AIN0, AIN1, AIN2, AIN3 channels. For further information check TLA2024's datasheet on docs/TLA2024.pdf 

As it is said, this library is used on the Halley Box's  TLA2024. These are the specific functions for this board's layout (To get further information about Halley Box's layout check out docs/HalleyBox.pdf):
* getVbat() : gets the voltage measure of the battery
* getVin() : gets the voltage measure of VIN
* getVsys() : gets the voltage measure of VSYS

## Library structure
 
These main functions use the other ones in order to achieve their purpose. There are two kinds of auxiliar functions, the private ones and the public ones:
- About the private ones, they are designed to write and read from the registers of the TLA2024. 
- About the public ones, they are used by the main functions to configure the parámeters of the TLA2024 or to convert the data from the register. 
For further information, every function has its own DOxygen comments which can be checked in src/rhio-tla2024.h
 
## License
 
This library was developed by rhomb.io and has a GNU/GPL v3 license. For more information, check the License file.

