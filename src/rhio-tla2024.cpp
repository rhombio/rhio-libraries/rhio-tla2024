#include "rhio-tla2024.h"

bool TLA2024::begin(uint8_t addr) {
  Wire.begin();
  Wire.setClock(100000);
  addr_I2C = addr;
  uint16_t init = readRegister(CONF_REG);
  if (init != INIT_CONF) {
    TLA2024::writeRegister(INIT_CONF, 0x0000);
    return false;
  }
  return true;
}

TLA2024::TLA2024() {}

void TLA2024::singleShotConversionStart() { writeRegister(0x8000, 0x7FFF); }

void TLA2024::setMux(uint16_t muxOption) { writeRegister(muxOption, 0x8FFF); }

void TLA2024::setPga(uint16_t pgaOption) { writeRegister(pgaOption, 0xF1FF); }

void TLA2024::reset(void) { writeRegister(INIT_CONF, 0x00); }

uint8_t TLA2024::getOS() {
  uint8_t OS = readBit(CONF_REG, 15);
  return OS;
}

uint16_t TLA2024::getMux() {
  uint16_t mux = readRegister(CONF_REG);
  mux &= 0x7000;
  mux >>= 12;
  return mux;
}

uint16_t TLA2024::getPga() {
  uint16_t pga = readRegister(CONF_REG);
  pga = pga & 0xE00;
  pga = pga >> 9;
  return pga;
}

float TLA2024::getInput(uint16_t muxOption) {
  uint16_t analogread = 0;
  setMux(muxOption);
  if (mode) {
    // write 1 to OS bit to start conv
    uint16_t current_conf = readRegister(CONF_REG);
    current_conf |= 0x8000;
    writeRegister(current_conf);
    // OS bit will be 0 until conv is done.
    do {
      delay(5);  // Wait until conversion is done
    } while (!getOS());
  }
  analogread = readRegister(CONV_REG);
  analogread >>= 4;  // Shift out CONV_REG reserved bits

  if (readBit(CONV_REG, 15)) {  // If bit 15 = 1 (Negative value)
    analogread |= 0xF000;
  } else {  // If bit 15 = 0 (Positive value)
    analogread &= 0x0FFF;
  }

  // Now store it as a signed 16 bit int.
  float ret = analogread;
  return ret;
}

float TLA2024::conversion(uint16_t analogread) {
  uint8_t sign = readBit(CONV_REG, 15);
  float analog = 0;
  switch (sign) {
    case 0:  // Positive analog read
      if (getPga() == 0) {
        analog = (float)analogread * 3;
      }
      if (getPga() == 1) {
        analog = (float)analogread * 2;
      }
      if (getPga() == 2) {
        analog = (float)analogread * 1;
      }
      if (getPga() == 3) {
        analog = (float)analogread * 0.5;
      }
      if (getPga() == 4) {
        analog = (float)analogread * 0.25;
      }

      if (getPga() == 5) {
        analog = (float)analogread * 0.125;
      }

      if (getPga() == 6) {
        analog = (float)analogread * 0.125;
      }

      if (getPga() == 7) {
        analog = (float)analogread * 0.125;
      }

      break;
    case 1:  // Negative analog read
      if (getPga() == 0) {
        analog = 6.144 - (float)analogread * 3;
        analog = 6.144 - analog;
      }
      if (getPga() == 1) {
        analog = 4.096 - (float)analogread * 2;
        analog = 4.096 - analog;
      }
      if (getPga() == 2) {
        analog = 2.048 - (float)analogread * 1;
        analog = 2.048 - analog;
      }
      if (getPga() == 3) {
        analog = 1.024 - (float)analogread * 0.5;
        analog = 1.024 - analog;
      }
      if (getPga() == 4) {
        analog = 0.521 - (float)analogread * 0.25;
        analog = 0.521 - analog;
      }

      if (getPga() == 5) {
        analog = 0.256 - (float)analogread * 0.125;
        analog = 0.256 - analog;
      }

      if (getPga() == 6) {
        analog = 0.256 - (float)analogread * 0.125;
        analog = 0.256 - analog;
      }

      if (getPga() == 7) {
        analog = 0.256 - (float)analogread * 0.125;
        analog = 0.256 - analog;
      }
      break;
  }

  return analog;
}

float TLA2024::getAnalog(uint16_t muxOption) {
  float analog = conversion(getInput(muxOption));
  return analog;
}

float TLA2024::getVbat() {
  float vbat = getAnalog(AIN0);
  vbat = (vbat * 5) / (1000 * 1.6);
  return vbat;
}

float TLA2024::getVin() {
  float vin = getAnalog(AIN1);
  vin = vin / (100 * 0.23);
  return vin;
}

float TLA2024::getVsys() {
  float vsys = getAnalog(AIN2);
  vsys = (vsys * 5) / (1000 * 1.6);
  return vsys;
}

uint8_t TLA2024::readBit(uint16_t regis, uint8_t move) {
  uint16_t movedRegis = readRegister(regis) >> move;
  uint8_t bit = movedRegis & 1;
  return bit;
}

uint16_t TLA2024::readRegister(uint16_t regis) {
  uint16_t result = 0, result1, result2;
  Wire.beginTransmission(addr_I2C);
  Wire.write(regis);
  Wire.endTransmission();
  delay(5);
  Wire.requestFrom(addr_I2C, 2u);
  if (2 <= Wire.available()) {
    result1 = Wire.read();
    result1 <<= 8;
    result2 = Wire.read();
    result = result1 | result2;
  }
  return result;
}

void TLA2024::writeRegister(uint16_t value, uint16_t mask) {
  int written = 0;
  if (mask != 0x00) {
    uint16_t conf = readRegister(CONF_REG);
    conf &= mask;
    value += conf;
  }
  data.value = value;
  Wire.beginTransmission(addr_I2C);
  Wire.write(CONF_REG);
  written += Wire.write(data.packet[1]);
  written += Wire.write(data.packet[0]);
  Wire.endTransmission();
  data.value = 0;
}
void TLA2024::setMode(bool mode) {
  // bring in conf reg
  TLA2024::mode = mode;
  uint16_t conf = readRegister(CONF_REG);
  // clear MODE bit (8) (continous conv)

  // Serial.println(conf, BIN);
  conf &= ~(1 << 8);
  // Serial.println(conf, BIN);
  if (mode) {
    // single shot
    conf |= (1 << 8);
  }
  writeRegister(conf);
}
