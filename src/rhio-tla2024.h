/**
 * Rhomb.io TLA2024 library
 *
 * @author César Tamarit Tamarit
 * @version 1.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */

#pragma once

#include <Arduino.h>
#include <Wire.h>

#define SINGLE true
#define CONT false

#define CONV_REG 0x00  // Conversion Data Register
#define CONF_REG 0x01  // Configuration Data Register

#define INIT_CONF 0x8583  // Default configuration

// MUX options:
#define AIN0AIN1 0
#define AIN0AIN3 4096
#define AIN1AIN3 8192
#define AIN2AIN3 12288
#define AIN0 16384
#define AIN1 20480
#define AIN2 24576
#define AIN3 28672

// I2C address deppending on your ADDR pin configuration:
#define ADDR_1 0x48  // ADDR pin connected to GND
#define ADDR_2 0x49  // ADDR pin connected to VDD
#define ADDR_3 0x4B  // ADDR pin connected to SCL

class TLA2024 {
 public:
  TLA2024();

  /** @fn bool begin(uint8_t addr);
   *  @brief Checks if TLA2024 works properly
   *  @return 0: Something went wrong
   *          1: Successfull initialization
   */
  bool begin(uint8_t addr);

  /** @fn void singleShotConversionStart();
   *  @brief Starts a single conversion. This function will work only in a
   *  power-down state and has no effect when a conversion is ongoing.
   */
  void singleShotConversionStart();

  /**
   *  @fn void setMux(uint16_t muxOption);
   *  @brief Configures the input multiplexer with muxOption.
   *  muxOption |            Result
   * -----------|-----------------------------------------
   *      0     |    AINP = AIN0 and AINN = AIN1(default)
   *   4096     |    AINP = AIN0 and AINN = AIN3
   *   8192     |    AINP = AIN1 and AINN = AIN3
   *  12288     |    AINP = AIN2 and AINN = AIN3
   *  16384     |    AINP = AIN0 and AINN = GND
   *  20480     |    AINP = AIN1 and AINN = GND
   *  24576     |    AINP = AIN2 and AINN = GND
   *  28672     |    AINP = AIN3 and AINN = GND
   * ------------------------------------------------------
   *  @param muxOption
   */
  void setMux(uint16_t muxOption);

  /**
   *  @fn void setPga(uint16_t pgaOption);
   *  @brief Sets the FSR of the programmable gain amplifier with pgaOption
   *  pgaOption |            Result
   * -----------|-------------------------------
   *      0     |    FSR = ±6.144 V
   *    512     |    FSR = ±4.096 V
   *   1024     |    FSR = ±2.048 V (default)
   *   1536     |    FSR = ±1.024 V
   *   2048     |    FSR = ±0.512 V
   *   2560     |    FSR = ±0.256 V
   *   3072     |    FSR = ±0.256 V
   *   3584     |    FSR = ±0.256 V
   *---------------------------------------------
   *  @param pgaOption
   */
  void setPga(uint16_t pgaOption);

  /** @fn void R_TLA2024::reset(void);
   *  @brief Writes the default value in the configuration register
   */
  void reset(void);

  /**
   * @fn uint8_t getOS();
   * @brief Gets the Operational Status
   * @return 0: The device is currently performing a conversion
   *         1: The device is not currently performing a conversion (default)
   */
  uint8_t getOS();

  /**
   * @fn uint16_t getMux();
   * @brief Gets the input multiplexer configuration
   * @return Value according to this table:
   *  muxOption |            Result
   * -----------|-----------------------------------------
   *      0     |    AINP = AIN0 and AINN = AIN1(default)
   *   4096     |    AINP = AIN0 and AINN = AIN3
   *   8192     |    AINP = AIN1 and AINN = AIN3
   *  12288     |    AINP = AIN2 and AINN = AIN3
   *  16384     |    AINP = AIN0 and AINN = GND
   *  20480     |    AINP = AIN1 and AINN = GND
   *  24576     |    AINP = AIN2 and AINN = GND
   *  28672     |    AINP = AIN3 and AINN = GND
   * ------------------------------------------------------
   */
  uint16_t getMux();

  /**
   * @fn uint16_t getPga();
   * @brief Gets the FSR of the programmable gain amplifier.
   * @return Value according to this table:
   *
   *   Return   |    Equivalent FSR
   * -----------|-------------------------------
   *      0     |    FSR = ±6.144 V
   *    512     |    FSR = ±4.096 V
   *   1024     |    FSR = ±2.048 V (default)
   *   1536     |    FSR = ±1.024 V
   *   2048     |    FSR = ±0.512 V
   *   2560     |    FSR = ±0.256 V
   *   3072     |    FSR = ±0.256 V
   *   3584     |    FSR = ±0.256 V
   *---------------------------------------------
   */
  uint16_t getPga();

  /**
   * @fn float getInput(uint16_t muxOption);
   * @brief Gets and processes the conversion register considering the analog
   * input defined with muxOption and the configured working mode. This
   * function is the first step to get the analog input. The next one will be
   * the function conversion(analogread).
   * @param muxOption
   * @return ret
   */
  float getInput(uint16_t muxOption);

  /**
   * @fn float conversion(uint16_t analogread);
   * @brief It processes the data acquired by getInput(muxOption) considering
   * the sign and the PGA. It is the step between getInput(muxOption) and
   * getContinuousAnalog(drOption, muxOption) or getSingleAnalog(muxOption).
   * @param muxOption
   * @return analog
   */
  float conversion(uint16_t analogread);

  /**
   * @fn float getAnalog(uint16_t muxOption);
   * @brief Gets a single measure of the analog input defined with
   * muxOption. It works with getInput(muxOption) and conversion(analogread).
   * @param muxOption
   * @return analog
   */
  float getAnalog(uint16_t muxOption);

  /**
   * @fn float getVbat();
   * @brief Gets a single measure of the battery voltage.
   * @return vbat
   */
  float getVbat();

  /**
   * @fn float getVin();
   * @brief Gets a single measure of Vin.
   * @return vin
   */
  float getVin();

  /**
   * @fn float getVsys();
   * @brief Gets a single measure of Vsys.
   * @return vsys
   */
  float getVsys();

  /**
   * @fn setMode(bool mode);
   * @brief Set the mode value
   * @param mode (SINGLE OR CONT)
   */
  void setMode(bool mode);

 private:
  union I2C_data {
    uint8_t packet[2];
    uint16_t value;
  } data;

  uint8_t addr_I2C;
  bool mode = SINGLE;

  /**
   * @fn uint8_t readBit(uint16_t regis, uint8_t move);
   * @brief Gets the value of a single bit
   * @param regis
   * @param move
   * @return bit
   */
  uint8_t readBit(uint16_t regis, uint8_t move);

  /**
   * @fn uint16_t readRegister(uint16_t regis);
   * @brief Gets the value of an entire register
   * @param regis
   * @return result
   */
  uint16_t readRegister(uint16_t regis);

  /**
   * @fn uint16_t writeRegister(uint16_t value, uint16_t mask);
   * @brief Writes an specific value in the configuration register
   * @param value
   * @param mask
   */
  void writeRegister(uint16_t value, uint16_t mask = 0x00);
};
